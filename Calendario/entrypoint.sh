#!/bin/bash
set -e

# modificamos el titulo de la pagina reemplazando el texto 'SITENAME' por el valor de la variable de entorno '$SITENAME


sed -i 's/SITENAME/'"$SITENAME"'/' /app/index.html



# Testear si dentro del directorio '/var/www/html' existe un fichero 'index.html'

if [ -f /var/www/html/index.html]; then
	
	exit 0
else
	cp /app/index.html /var/www/html
fi

exec "$@"

